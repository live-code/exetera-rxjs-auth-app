import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { first } from 'rxjs/operators';

export interface Config {
  theme: string;
  fontSize: number;
  language: 'it' | 'en';
}

const INITIAL_STATE: Config = {
  theme: 'dark',
  language: 'en',
  fontSize: 20
};

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private data$: BehaviorSubject<Config> = new BehaviorSubject<Config>(INITIAL_STATE);
  myData$ = this.data$.asObservable();

  update(value: Partial<Config>): void {
    this.data$
      .pipe(first())
      .subscribe(config => this.data$.next({...config, ...value}));
  }

}

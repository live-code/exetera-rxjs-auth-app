import { Component } from '@angular/core';
import { ConfigService } from '../../core/services/config.service';

@Component({
  selector: 'app-settings',
  template: `
    
    <input 
      type="number" name="fontSize" 
      [ngModel]="(configService.myData$ | async).fontSize"
      (ngModelChange)="updateFontSize($event)"
    >
  
    <button (click)="configService.update({ theme: 'dark'})">Dark</button>
    <button (click)="configService.update({ theme: 'light'})">light</button>
  `,
})
export class SettingsComponent {

  constructor(public configService: ConfigService) { }

  submitHandler(value: any): void {
    this.configService.update(value);
  }

  updateFontSize(fontSize: number): void {
    this.configService.update({ fontSize });

  }
}

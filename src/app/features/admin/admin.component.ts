import { Component, OnDestroy, OnInit } from '@angular/core';
import { ConfigService } from '../../core/services/config.service';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../core/auth/auth.service';

@Component({
  selector: 'app-admin',
  template: `
    <h1>Admin</h1>
    <div>{{configService.myData$ | async | json}}</div>
    <button (click)="getUser()">get users</button>
    
    {{authService.resize$ | async | json}}
    
    <br>
    <span 
      style="background-color: red; display: inline-block"
      [style.width.px]="(authService.resize$ | async).width">
      bla bla
    </span>
  `,
})
export class AdminComponent implements OnDestroy {
  destroy$: Subject<void> = new Subject<void>();

  constructor(
    public authService: AuthService,
    public configService: ConfigService, private http: HttpClient) {
    configService.myData$
      .pipe(takeUntil(this.destroy$))
      .subscribe(val => console.log(val));


  }

  getUser(): void {
    this.http.get('http://localhost:3000/users')
      .subscribe()
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

}

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
  ) {
  }

  canActivate(): Observable<boolean> {

    return this.authService.isLogged$()
      .pipe(
        tap(isLogged => {
          if (!isLogged) {
            this.router.navigateByUrl('login')
          }
        })
      );
  }

  // server side version
  /*
  canActivate2(): Observable<boolean> {
    return this.authService.token$
      .pipe(
        switchMap(token => this.authService.validateToken(token)),
        tap(res => {
          if (res.response !== 'ok') {
            this.router.navigateByUrl('login')
          }
        }),
        map(res => res.response === 'ok')
      );
  }
  */
}

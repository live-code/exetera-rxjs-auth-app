import { Directive, ElementRef, HostBinding, Input, OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from './auth.service';
import { filter, takeUntil, tap, withLatestFrom } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Directive({
  selector: '[appIfLogged]'
})
export class IfLoggedDirective implements OnDestroy {
  @Input() appIfLogged: string;

  destroy$ = new Subject();

  constructor(
    private authService: AuthService,
    view: ViewContainerRef,
    template: TemplateRef<any>
  ) {
    this.authService.isLogged$()
      .pipe(
        takeUntil(this.destroy$),
        tap(() => view.clear()),
        withLatestFrom(this.authService.role$),
        filter(([isLogged, role]) => {
          return (isLogged && !this.appIfLogged) ||
                  isLogged && role === this.appIfLogged;
        })
      )
      .subscribe(() => {
        view.createEmbeddedView(template);
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

}

import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { iif, Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { catchError, first, map, mergeMap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.token$
      .pipe(
        first(),
        mergeMap(token => iif(
          () => !!token,
          next.handle(request.clone({ setHeaders: { Authorization: token } })),
          next.handle(request)
        )),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 404:
                break;

              case 401:
              default:
                console.log('redirect to login');
                this.authService.logout();
                this.router.navigateByUrl('login');
                break;
            }
          }
          return of(err)
        })
      );
  }
}

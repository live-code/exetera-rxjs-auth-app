import { Component, OnInit } from '@angular/core';
import { Config, ConfigService } from '../../services/config.service';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-navbar',
  template: `
    
    <div 
      style="padding: 20px; color: white" 
      *ngIf="configService.myData$ | async as config"
      [style.fontSize.px]="config?.fontSize"
      [style.backgroundColor]="config?.theme === 'dark' ? 'black' : 'grey'"
    >
      <div style="color: white">Exetera</div>
      <button routerLink="login">login</button>
      <button routerLink="admin" *appIfLogged="'admin'">admin</button>
     
      <button routerLink="settings" *appIfLogged >settings</button>
      
      
      <button (click)="authservice.logout()">Logout</button>
      {{config.language}}
      {{authservice.displayName$ | async }}
    </div>
   
  `,
})
export class NavbarComponent implements OnInit {

  constructor(public authservice: AuthService, public configService: ConfigService) {
  }

  ngOnInit(): void {
  }

}

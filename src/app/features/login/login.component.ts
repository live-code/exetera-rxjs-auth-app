import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  template: `
    <button (click)="authservice.login('a', 'b')">Login</button>
  `,
})
export class LoginComponent implements OnInit, OnDestroy {
  sub: Subscription;

  constructor(public authservice: AuthService, private router: Router){ }

  ngOnInit(): void {
    this.sub = this.authservice.isLogged$()
      .pipe(
        filter(isLogged => !!isLogged)
      )
      .subscribe(() => {
        this.router.navigateByUrl('admin');
      });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject, fromEvent, Observable } from 'rxjs';
import { Auth } from './auth';
import { map, shareReplay } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth$: BehaviorSubject<Auth> = new BehaviorSubject<Auth>(null);
  resize$: BehaviorSubject<any> = new BehaviorSubject<any>({
    width: window.innerWidth,
    height: window.innerHeight,
  });

  constructor(private http: HttpClient) {
    fromEvent(window, 'resize')
      .pipe(
        map((event: any) => ({ width: event.target.innerWidth, height: event.target.innerHeight})),
        // shareReplay()
      )
      .subscribe(this.resize$)

  }
  login(user: string, pass: string): void {
    // Api
    // const response: Auth =  { role: 'pippo', token: 'pluto', displayName: 'Topolino'};
    this.http.get<Auth>('http://localhost:3000/login')
      .subscribe(res => this.auth$.next(res))
  }


  validateToken(token: string): Observable<{response: string}> {
    return this.http.get<{response: string}>('http://localhost:3000/validateToken?tk=' + token);
  }

  logout(): void {
    this.auth$.next(null);
  }

  isLogged$(): Observable<boolean> {
    return this.auth$
      .pipe(
        map(val => !!val)
      );
  }

  get token$(): Observable<string> {
    return this.auth$
      .pipe(
        map(val => val ? val.token : null)
      );
  }

  get displayName$(): Observable<string> {
    return this.auth$
      .pipe(
        map(val => val?.displayName)
      );
  }

  get role$(): Observable<string> {
    return this.auth$
      .pipe(
        map(val => val?.role)
      );
  }
}

